#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import * as s3 from 'aws-cdk-lib/aws-s3'
import { TestingStack } from '../lib/testing-stack';
import { IConstruct } from 'constructs';


class BucketVersioningChecker implements cdk.IAspect {
  public visit(node: IConstruct): void {
    if (node instanceof s3.CfnBucket) {

      if (!node.versioningConfiguration
          || (!cdk.Tokenization.isResolvable(node.versioningConfiguration)
              && node.versioningConfiguration.status !== 'Enabled')) {

        cdk.Annotations.of(node).addWarningV2("S3_POLICY", "Bucket does not have cersioning enabled")
        node.applyRemovalPolicy(cdk.RemovalPolicy.RETAIN)// OR throw an ERROR
        // cdk.Annotations.of(node).addError('Bucket versioning is not enabled');

      }
    }
  }
}


const app = new cdk.App();

const stack = new TestingStack(app, 'TestingStack');

cdk.Aspects.of(stack).add(new BucketVersioningChecker());

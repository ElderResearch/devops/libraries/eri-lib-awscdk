import { Construct } from 'constructs';
import { ComplianceConfig } from './compliance'
import * as aws_ec2 from 'aws-cdk-lib/aws-ec2'

export interface SecureBucketStackProps {
  complianceProperties: ComplianceConfig
}

interface SecureVpcProps {
  complianceProperties: ComplianceConfig
}

export class SecureVpc extends Construct {
  public readonly vpc: aws_ec2.Vpc
  constructor(scope: Construct, id: string, props: SecureVpcProps) {
    super(scope, id);

    const vpc = new aws_ec2.Vpc(this, "secureVPC", {
      flowLogs:{
        'destination': {
          destination: aws_ec2.FlowLogDestination.toS3(props.complianceProperties.bucket, this.node.id, { fileFormat: aws_ec2.FlowLogFileFormat.PARQUET}),
        }
      },
      availabilityZones:[],
    })
    vpc.addGatewayEndpoint("Service", {
      service: aws_ec2.GatewayVpcEndpointAwsService.S3
    })
  }
}

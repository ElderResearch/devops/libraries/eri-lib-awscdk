import { Construct } from 'constructs';
import * as aws_s3 from 'aws-cdk-lib/aws-s3'
import * as aws_cloudtrail from 'aws-cdk-lib/aws-cloudtrail'
import * as aws_logs from 'aws-cdk-lib/aws-logs'
import * as aws_kms from 'aws-cdk-lib/aws-kms'


interface CloudwatchS3TrailStackProps {
  bucket: aws_s3.Bucket
}

export class S3DataTrailStack extends Construct {
  public kmsKey: aws_kms.Key
  constructor(scope: Construct, id: string, props: CloudwatchS3TrailStackProps) {
    super(scope, id);

    const trail = new aws_cloudtrail.Trail(this, "S3DataTrail", {
      managementEvents: aws_cloudtrail.ReadWriteType.NONE,
      enableFileValidation: true,
      sendToCloudWatchLogs: true,
      cloudWatchLogsRetention: aws_logs.RetentionDays.THREE_MONTHS,
    })
    trail.addS3EventSelector([{
      bucket: props.bucket
    }],{
      readWriteType: aws_cloudtrail.ReadWriteType.ALL
    })
  }
}

export class S3ManagementTrailStack extends Construct {
  public kmsKey: aws_kms.Key
  constructor(scope: Construct, id: string, props: CloudwatchS3TrailStackProps) {
    super(scope, id);
    const trail = new aws_cloudtrail.Trail(this, "S3ManagementTrail", {
      managementEvents: aws_cloudtrail.ReadWriteType.ALL,
      enableFileValidation: true,
      sendToCloudWatchLogs: true,
      cloudWatchLogsRetention: aws_logs.RetentionDays.THREE_MONTHS,
      insightTypes: [aws_cloudtrail.InsightType.API_CALL_RATE, aws_cloudtrail.InsightType.API_ERROR_RATE]
    })
    trail.addS3EventSelector([{
      bucket: props.bucket
    }])
  }
}

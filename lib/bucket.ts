import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as aws_ec2 from 'aws-cdk-lib/aws-ec2'
import * as aws_s3 from 'aws-cdk-lib/aws-s3'
import * as aws_kms from 'aws-cdk-lib/aws-kms'
import * as aws_iam from 'aws-cdk-lib/aws-iam'
import { S3DataTrailStack, S3ManagementTrailStack } from './trail'
import { ComplianceConfig } from './compliance'


export interface SecureBucketStackProps {
  complianceProperties: ComplianceConfig
}

export class SecureBucketStack extends Construct {
  public readonly bucket: aws_s3.Bucket
  constructor(scope: Construct, id: string, props: SecureBucketStackProps) {
    super(scope, id);

    this.bucket = new aws_s3.Bucket(this, "LogBucket", {
      publicReadAccess: false,
      removalPolicy: props.complianceProperties.stage.removalPolicy,
      lifecycleRules:[{expiration: props.complianceProperties.stage.lifecycleRuleExpiration}],
      intelligentTieringConfigurations: [{name: this.node.id}],
      metrics: [{id: this.node.id}],
      blockPublicAccess: aws_s3.BlockPublicAccess.BLOCK_ALL,
      encryption: aws_s3.BucketEncryption.KMS,
      objectLockEnabled: true,
      objectLockDefaultRetention: aws_s3.ObjectLockRetention.governance(cdk.Duration.days(props.complianceProperties.objectLockRetention)),
      eventBridgeEnabled: true,
      enforceSSL: true,
      encryptionKey: props.complianceProperties.kmsKey
    })

    const inventoryDestination = new aws_s3.Bucket(this, "InventoryDestination")

    this.bucket.addInventory({
      format: aws_s3.InventoryFormat.PARQUET,
      enabled: true,
      frequency: aws_s3.InventoryFrequency.DAILY,
      includeObjectVersions: aws_s3.InventoryObjectVersion.CURRENT,
      destination: { bucket:  inventoryDestination}
    })

    const vpcOnlyDataAccess = new aws_iam.PolicyStatement({
      effect: aws_iam.Effect.DENY,
      actions: ["s3:PutObject", "s3:GetObject", "s3:DeleteObject"],
      resources: ["*"],
      principals: [new aws_iam.AnyPrincipal()],
      conditions: {
        "StringNotEquals": {
          "aws:PrincipalTag/DataAccessLevel": props.complianceProperties.dataAccessLevel
        }
      }
    })
    this.bucket.addToResourcePolicy(vpcOnlyDataAccess)
    cdk.Tags.of(this).add("DataAccessLevel", props.complianceProperties.dataAccessLevel)

    new S3DataTrailStack(this, "S3DataEventsTrail", {
      bucket: this.bucket
    })
    new S3ManagementTrailStack(this, "S3ManagementEventsTrail", {
      bucket: this.bucket
    })
  }
}

export class VpcOnlyBucketStack extends Construct {
  public kmsKey: aws_kms.Key
  public readonly bucket: aws_s3.Bucket
  public vpc: aws_ec2.Vpc
  constructor(scope: Construct, id: string) {
    super(scope, id);
    const vpcOnlyDataAccess = new aws_iam.PolicyStatement({
      effect: aws_iam.Effect.DENY,
      actions: ["s3:PutObject", "s3:GetObject", "s3:DeleteObject"],
      resources: ["*"],
      principals: [new aws_iam.AnyPrincipal()],
      conditions: {
        "StringNotEquals": {
          "aws:sourceVpc": this.vpc.vpcId
        }
      }
    })
    this.bucket.addToResourcePolicy(vpcOnlyDataAccess)
  }
}

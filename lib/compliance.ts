import { Construct } from 'constructs';
import * as aws_s3 from 'aws-cdk-lib/aws-s3'
import * as aws_kms from 'aws-cdk-lib/aws-kms'
import * as aws_iam from 'aws-cdk-lib/aws-iam'
import * as cdk from 'aws-cdk-lib';
import * as aws_sns from 'aws-cdk-lib/aws-sns'
import * as aws_sns_subscriptions from 'aws-cdk-lib/aws-sns-subscriptions'
import * as aws_backup from 'aws-cdk-lib/aws-backup'
import { AccessLevel, StageProp } from './types'
import { S3DataTrailStack, S3ManagementTrailStack } from './trail'

export interface ComplianceProps {
  objectLockRetention: number
  dataAccessLevel: AccessLevel
  stage: StageProp
  kmsKey: aws_kms.Key
  applicationName: string
}

class SecureBucketStack extends Construct {
  public readonly bucket: aws_s3.Bucket
  constructor(scope: Construct, id: string, props: ComplianceProps) {
    super(scope, id);

    this.bucket = new aws_s3.Bucket(this, "LogBucket", {
      publicReadAccess: false,
      removalPolicy: props.stage.removalPolicy,
      lifecycleRules:[{expiration: props.stage.lifecycleRuleExpiration}],
      intelligentTieringConfigurations: [{name: this.node.id}],
      metrics: [{id: this.node.id}],
      blockPublicAccess: aws_s3.BlockPublicAccess.BLOCK_ALL,
      encryption: aws_s3.BucketEncryption.KMS,
      objectLockEnabled: true,
      objectLockDefaultRetention: aws_s3.ObjectLockRetention.governance(cdk.Duration.days(props.objectLockRetention)),
      eventBridgeEnabled: true,
      enforceSSL: true,
      encryptionKey: props.kmsKey
    })

    const inventoryDestination = new aws_s3.Bucket(this, "InventoryDestination")

    this.bucket.addInventory({
      format: aws_s3.InventoryFormat.PARQUET,
      enabled: true,
      frequency: aws_s3.InventoryFrequency.DAILY,
      includeObjectVersions: aws_s3.InventoryObjectVersion.CURRENT,
      destination: { bucket:  inventoryDestination}
    })

    const vpcOnlyDataAccess = new aws_iam.PolicyStatement({
      effect: aws_iam.Effect.DENY,
      actions: ["s3:PutObject", "s3:GetObject", "s3:DeleteObject"],
      resources: ["*"],
      principals: [new aws_iam.AnyPrincipal()],
      conditions: {
        "StringNotEquals": {
          "aws:PrincipalTag/dataaccesslevel": props.dataAccessLevel
        }
      }
    })
    this.bucket.addToResourcePolicy(vpcOnlyDataAccess)
    cdk.Tags.of(this).add("DataAccessLevel", props.dataAccessLevel)

    new S3DataTrailStack(this, "S3DataEventsTrail", {
      bucket: this.bucket
    })
    new S3ManagementTrailStack(this, "S3ManagementEventsTrail", {
      bucket: this.bucket
    })
  }
}

export class ComplianceConfig extends Construct {
  public readonly bucket: aws_s3.Bucket
  public readonly stage: StageProp
  public readonly kmsKey: aws_kms.Key
  public readonly dataAccessLevel: AccessLevel
  public readonly objectLockRetention: number
  public readonly alarmTopic: aws_sns.Topic
  public readonly backupPlan: aws_backup.BackupPlan
  constructor(scope: Construct, id: string, props: ComplianceProps) {
    super(scope, id)
    cdk.Tags.of(this).add("dataaccesslevel", props.dataAccessLevel)

    this.backupPlan = new aws_backup.BackupPlan(this, "Plan")
    new aws_backup.BackupVault(this, "Vault", {
      backupVaultName: props.applicationName,
      removalPolicy: props.stage.removalPolicy,
      encryptionKey: props.kmsKey
    })
    this.backupPlan.addRule(new aws_backup.BackupPlanRule({
      enableContinuousBackup: true,
      deleteAfter: cdk.Duration.days(35)
    }))

    const secureBucket = new SecureBucketStack(this, "SecureLogsBucket", {
      kmsKey: props.kmsKey,
      dataAccessLevel: props.dataAccessLevel,
      stage: props.stage,
      objectLockRetention: props.objectLockRetention,
      applicationName: props.applicationName
    })

    this.alarmTopic = new aws_sns.Topic(this, "AlarmTopic", {
      fifo: true,
      topicName:props.applicationName
    })
    this.alarmTopic.addSubscription(new aws_sns_subscriptions.EmailSubscription(`support+${props.applicationName}@elderresearch.com`))
    this.bucket = secureBucket.bucket
    this.kmsKey = props.kmsKey
    this.stage = props.stage
    this.dataAccessLevel = props.dataAccessLevel
    this.objectLockRetention = props.objectLockRetention
  }
}

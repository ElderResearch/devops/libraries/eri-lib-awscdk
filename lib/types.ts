import * as cdk from 'aws-cdk-lib';
import * as aws_rds from 'aws-cdk-lib/aws-rds'

export interface StageProp {
  name: string,
  removalPolicy: cdk.RemovalPolicy,
  lifecycleRuleExpiration: cdk.Duration
  performanceInsightRetention: aws_rds.PerformanceInsightRetention
  rdsDeletionProtection: boolean
  rdsDeleteAutomatedBackups: boolean
}

class Dev implements StageProp {
  name = "dev"
  removalPolicy = cdk.RemovalPolicy.DESTROY
  lifecycleRuleExpiration = cdk.Duration.days(30)
  performanceInsightRetention: cdk.aws_rds.PerformanceInsightRetention.MONTHS_1
  rdsDeletionProtection: false
  rdsDeleteAutomatedBackups: true
}

class Stage implements StageProp {
  name = "stage"
  removalPolicy = cdk.RemovalPolicy.DESTROY
  lifecycleRuleExpiration = cdk.Duration.days(30)
  performanceInsightRetention: cdk.aws_rds.PerformanceInsightRetention.MONTHS_1
  rdsDeletionProtection: false
  rdsDeleteAutomatedBackups: true
}

class Prod implements StageProp {
  name = "prod"
  removalPolicy = cdk.RemovalPolicy.RETAIN
  lifecycleRuleExpiration = cdk.Duration.days(90)
  performanceInsightRetention: cdk.aws_rds.PerformanceInsightRetention.MONTHS_12
  rdsDeletionProtection: true
  rdsDeleteAutomatedBackups: false
}

export const Stages: {"DEV": StageProp, "STAGE": StageProp, "PROD": StageProp} = {
  DEV: new Dev(),
  STAGE: new Stage(),
  PROD: new Prod()
}

export enum AccessLevel {
  LOW = "low",
  MEDIUM = "medium",
  HIGH = "high"
}

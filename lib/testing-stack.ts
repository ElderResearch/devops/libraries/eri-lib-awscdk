import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as aws_kms from 'aws-cdk-lib/aws-kms'
import { AccessLevel, Stages } from './types'
import { ComplianceConfig } from './compliance'
import { SecureVpc } from './vpc';

export class TestStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const complianceConfig = new ComplianceConfig(this, "Config", {
      dataAccessLevel: AccessLevel.MEDIUM,
      kmsKey: new aws_kms.Key(this, "ApplicationKey"),
      objectLockRetention: 30,
      stage: Stages.DEV
    })

    new SecureVpc(this, "MySecureVPC", {
      complianceProperties: complianceConfig
    })

    new SecureVpc(this, "MySecureVPCTwo", {
      complianceProperties: complianceConfig
    })
  }
}

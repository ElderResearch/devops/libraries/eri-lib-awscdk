import * as cdk from 'aws-cdk-lib'
import * as aws_ec2 from 'aws-cdk-lib/aws-ec2'
import * as aws_rds from 'aws-cdk-lib/aws-rds'
import * as aws_iam from 'aws-cdk-lib/aws-iam'
import * as aws_backup from 'aws-cdk-lib/aws-backup'
import { Construct } from 'constructs';
import { SecureBucketStack } from './bucket'
import { ComplianceConfig } from './compliance'
import * as aws_cloudwatch from 'aws-cdk-lib/aws-cloudwatch'
import * as aws_cloudwatch_actions from 'aws-cdk-lib/aws-cloudwatch-actions'

interface SecureRdsProps {
    complianceConfig: ComplianceConfig
    allocatedStorage: number
    databaseName: string
    userName: string
    instanceIdentifier: string
    backupRetentionDays: number
    timezone: string
    vpc: aws_ec2.Vpc
    vpcSubnets: aws_ec2.SubnetSelection
    postgresVersion: aws_rds.PostgresEngineVersion
    instanceClass: aws_ec2.InstanceClass
    instanceSize: aws_ec2.InstanceSize
    databaseSubnetGroup: aws_rds.SubnetGroup
    securityGroups: aws_ec2.SecurityGroup[]
    storageType: aws_rds.StorageType
}

export class SecureRDSPostgres extends Construct {
    public readonly instance: aws_rds.DatabaseInstance
    public readonly storageAlarm: aws_cloudwatch.Alarm
    public readonly cpuAlarm: aws_cloudwatch.Alarm
    constructor(scope: Construct, id: string, props: SecureRdsProps) {
        super(scope, id)

        cdk.Tags.of(this).add("dataaccesslevel", props.complianceConfig.dataAccessLevel)

        const backupBucket = new SecureBucketStack(this, "ExportBucket", {
            complianceProperties: props.complianceConfig
        })

        const permissionsBoundary = new aws_iam.ManagedPolicy(this, 'Boundry',{
            statements: [
                new aws_iam.PolicyStatement({
                    effect: aws_iam.Effect.DENY,
                    actions: ['iam:*'],
                    resources: ['*']
                })
            ]
        })

        const exportRole = new aws_iam.Role(this, "ExportRole", {
            roleName: `rds-export-role-${props.instanceIdentifier}`,
            assumedBy: new aws_iam.ServicePrincipal('rds.amazonaws.com'),
            description:"Role used by RDS to export Backups.",
            permissionsBoundary: permissionsBoundary
        })
        backupBucket.bucket.grantReadWrite(exportRole)

        this.instance = new aws_rds.DatabaseInstance(this, 'SecureRdsInstance', {
            allocatedStorage: props.allocatedStorage,
            allowMajorVersionUpgrade: false,
            autoMinorVersionUpgrade: true,
            backupRetention: cdk.Duration.days(props.backupRetentionDays),
            credentials: aws_rds.Credentials.fromGeneratedSecret(props.userName, {
                encryptionKey: props.complianceConfig.kmsKey,
                secretName: `${props.instanceIdentifier}-admin`,
            }),
            s3ExportBuckets: [backupBucket.bucket],
            //s3ExportRole: exportRole,
            databaseName: props.databaseName,
            engine: aws_rds.DatabaseInstanceEngine.postgres({
                version: props.postgresVersion,
            }),
            enablePerformanceInsights: true,
            performanceInsightRetention: props.complianceConfig.stage.performanceInsightRetention,
            iamAuthentication: true,
            instanceIdentifier: props.instanceIdentifier,
            instanceType: aws_ec2.InstanceType.of(
                props.instanceClass,
                props.instanceSize
            ),
            removalPolicy: props.complianceConfig.stage.removalPolicy,
            deletionProtection: props.complianceConfig.stage.rdsDeletionProtection,
            deleteAutomatedBackups: props.complianceConfig.stage.rdsDeleteAutomatedBackups,
            storageEncrypted: true,
            storageEncryptionKey: props.complianceConfig.kmsKey,
            storageType: aws_rds.StorageType.GP2,
            publiclyAccessible: false,
            vpc: props.vpc,
            securityGroups: props.securityGroups,
            subnetGroup: props.databaseSubnetGroup,
            vpcSubnets: { subnets: props.vpc.privateSubnets },
            port: 5432
        });

        const storage_alarm = new aws_cloudwatch.Alarm(this, "StorageAlarm", {
            metric: this.instance.metricFreeStorageSpace(),
            evaluationPeriods: 30,
            threshold: 50,
            alarmName: `alarm-${props.instanceIdentifier}-storage`,
            actionsEnabled: true,
            datapointsToAlarm: 10,
            comparisonOperator: aws_cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD
        })
        storage_alarm.addAlarmAction(new aws_cloudwatch_actions.SnsAction(props.complianceConfig.alarmTopic))

        const cpu_alarm = new aws_cloudwatch.Alarm(this, "CPUAlarm", {
            metric: this.instance.metricCPUUtilization(),
            evaluationPeriods: 30,
            threshold: 80,
            alarmName: `alarm-${props.instanceIdentifier}-cpu`,
            actionsEnabled: true,
            datapointsToAlarm: 10,
            comparisonOperator: aws_cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD
        })
        cpu_alarm.addAlarmAction(new aws_cloudwatch_actions.SnsAction(props.complianceConfig.alarmTopic))
        props.complianceConfig.backupPlan.addSelection("Selection", {
            resources: [
                aws_backup.BackupResource.fromRdsDatabaseInstance(this.instance)
            ]
        })
    }
}

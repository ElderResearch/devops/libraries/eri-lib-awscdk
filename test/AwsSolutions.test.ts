import { execSync } from "child_process";
import * as cdk from 'aws-cdk-lib';
import * as aws_ec2 from 'aws-cdk-lib/aws-ec2'
import * as aws_kms from 'aws-cdk-lib/aws-kms'
import * as aws_rds from 'aws-cdk-lib/aws-rds'
import { Construct } from 'constructs';
import { AwsSolutionsChecks } from 'cdk-nag'
import { Aspects } from 'aws-cdk-lib';
import { SecureRDSPostgres } from '../../library/lib/rds'
import { ComplianceConfig } from '../../library/lib/compliance'
import { AccessLevel, Stages } from '../../library/lib/types'


export class RDSPostgresCheck extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const config = new ComplianceConfig(this, "CompianceConfig", {
      applicationName: "RDSTest",
      dataAccessLevel: AccessLevel.HIGH,
      kmsKey: new aws_kms.Key(this, "DataKey"),
      objectLockRetention: 90,
      stage: Stages.PROD
    })

    const vpc = new aws_ec2.Vpc(this, "MyVPC", {
      subnetConfiguration:[
        {
          name: "yankydoodle",
          subnetType: aws_ec2.SubnetType.PRIVATE_ISOLATED
        }
      ],
      flowLogs:{
        "destination": { destination: aws_ec2.FlowLogDestination.toCloudWatchLogs()}
      }
    })
    const subnetGroup = new aws_rds.SubnetGroup(this, "DatabaseSubnet",{
      vpc: vpc,
      description: "Mygroup",
      vpcSubnets: {
        subnets: vpc.isolatedSubnets
      }
    })

    new SecureRDSPostgres(this, "SecureRDS", {
      allocatedStorage: 10,
      backupRetentionDays: 90,
      complianceConfig: config,
      databaseName: "MyDatabase",
      databaseSubnetGroup: subnetGroup,
      instanceClass: aws_ec2.InstanceClass.BURSTABLE4_GRAVITON,
      instanceIdentifier: "SingingInTheRain",
      instanceSize: aws_ec2.InstanceSize.MEDIUM,
      postgresVersion: aws_rds.PostgresEngineVersion.VER_14_9,
      vpc: vpc,
      userName: "testing",
      storageType: aws_rds.StorageType.GP2,
      securityGroups: [new aws_ec2.SecurityGroup(this, "mySecurityGroup",{
        vpc: vpc,
      })],
      vpcSubnets: {
        subnets: vpc.isolatedSubnets
      },
      timezone: "",
    })
  }
}




describe('AWS Solutions', ()=>{
    it('RDS Postgres test', () => {
      const app = new cdk.App();
      Aspects.of(app).add(new AwsSolutionsChecks({ verbose: true }))
      const sd = new RDSPostgresCheck(app, 'AwsSolutionsStack');
      const annotations = cdk.assertions.Annotations.fromStack(sd);
      expect(Object.keys(annotations['_messages']).length).toEqual(0)
    });

    it('Some other test', () => {
      const app = new cdk.App();
      Aspects.of(app).add(new AwsSolutionsChecks({ verbose: true }))
      const sd = new RDSPostgresCheck(app, 'AwsSolutionsStack');
      const annotations = cdk.assertions.Annotations.fromStack(sd);
      expect(Object.keys(annotations['_messages']).length).toEqual(0)
    });
})
